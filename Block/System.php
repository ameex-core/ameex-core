<?php
namespace Ameex\Core\Block;
/**
* Ameex_Core extension
* @category Ameex
* @package Ameex_Core
* @copyright Copyright (c) 2018
* @license Open Software License (OSL 3.0)
*/
use Magento\Framework\App\State;
use Magento\Framework\Data\Form\Element\AbstractElement;

class System extends \Magento\Config\Block\System\Config\Form\Fieldset
{
   
    private $_layoutFactory;  
    private $directoryList;    
    public function __construct(
        \Magento\Backend\Block\Context $context,
        \Magento\Framework\App\State $appState,
        \Magento\Backend\Model\Auth\Session $authSession,
        \Magento\Framework\View\Helper\Js $jsHelper,
        \Magento\Framework\View\LayoutFactory $layoutFactory,        
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        array $data = array()
    ) {
        parent::__construct($context, $authSession, $jsHelper, $data);
        $this->_appState = $appState;
        $this->_layoutFactory = $layoutFactory; 
        $this->directoryList = $directoryList;
    }

    public function render(AbstractElement $element)
    {
        $html = $this->_getHeaderHtml($element);
        $html .= $this->getMagentoMode($element);
/*        $html .= $this->getMagentoPathInfo($element);
*/        $html .= $this->_getFooterHtml($element);
        return $html;
    }

   
    private function _getFieldRenderer()
    {
        if (empty($this->_fieldRenderer)) {
            $layout = $this->_layoutFactory->create();

            $this->_fieldRenderer = $layout->createBlock(
                \Magento\Config\Block\System\Config\Form\Field::class
            );
        }

        return $this->_fieldRenderer;
    }
    private function getMagentoMode($fieldset)
    {
        $label = __("Magento Mode");
        $mode = $this->_appState->getMode();
        return $this->getFieldHtml($fieldset, 'magento_mode', $label, $mode);
    }
 /*   private function getMagentoPathInfo($fieldset)
    {
        $label = __("Magento Path");
        $path = $this->directoryList->getRoot();

        return $this->getFieldHtml($fieldset, 'magento_path', $label, $path);
    } */
    private function getFieldHtml($fieldset, $fieldName, $label = '', $value = '')
    {
        $field = $fieldset->addField(
            $fieldName, 'label', array(
            'name'  => 'temp',
            'label' => $label,
            'after_element_html' => $value,
            )
        )->setRenderer($this->_getFieldRenderer());

        return $field->toHtml();
    }
}
