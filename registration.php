<?php
/**
* Ameex_Core extension
* @category Ameex
* @package Ameex_Core
* @copyright Copyright (c) 2018
* @license Open Software License (OSL 3.0)
*/

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Ameex_Core',
    __DIR__
);